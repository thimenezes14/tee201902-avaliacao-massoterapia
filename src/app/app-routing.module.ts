import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)},
  {
    path: 'agendar-servico',
    loadChildren: () => import('./pages/agendar-servico/agendar-servico.module').then( m => m.AgendarServicoPageModule)
  },
  {
    path: 'agendar-servico/profissional',
    loadChildren: () => import('./pages/profissional/profissional.module').then( m => m.ProfissionalPageModule)
  },
  {
    path: 'agendar-servico/data-agendamento',
    loadChildren: () => import('./pages/data-agendamento/data-agendamento.module').then( m => m.DataAgendamentoPageModule)
  },
  {
    path: 'agendar-servico/pagamento',
    loadChildren: () => import('./pages/pagamento/pagamento.module').then( m => m.PagamentoPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
