import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { ToolbarComponent } from './toolbar/toolbar.component';



@NgModule({
  declarations: [ToolbarComponent],
  imports: [
    IonicModule,
    CommonModule
  ],
  exports: [
    ToolbarComponent
  ]
})
export class ToolbarModule { }