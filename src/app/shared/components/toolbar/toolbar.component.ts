import { Component, OnInit, Input } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit {

  userName: string;
  userPhoto: string;

  @Input()
  titulo: string;

  constructor(private navCtrl: NavController) {}

  ngOnInit() {
    
    let dataAuth = JSON.parse(localStorage.getItem('auth'));

    if(!dataAuth){
      this.navCtrl.navigateRoot('/');
      return;
    }
  
    if(!('displayName' && 'photoURL' in dataAuth)){
      this.navCtrl.navigateRoot('/');
      return;
    }
      
    this.userName = dataAuth.displayName;
    this.userPhoto = dataAuth.photoURL;

  }

  logout() {
    localStorage.removeItem('auth');
    this.navCtrl.navigateRoot('/');
  }

}
