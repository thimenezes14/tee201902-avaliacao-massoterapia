import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import {HttpClientModule} from '@angular/common/http';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import {AngularFireModule} from '@angular/fire';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {AngularFireAuthModule} from '@angular/fire/auth';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import * as firebase from 'firebase';

var firebaseConfig = {
  apiKey: "AIzaSyDEe2060rBHtpf7XvdqFR2n3IL2PYOOJX8",
  authDomain: "tee1902-avaliacao-massoterapia.firebaseapp.com",
  databaseURL: "https://tee1902-avaliacao-massoterapia.firebaseio.com",
  projectId: "tee1902-avaliacao-massoterapia",
  storageBucket: "tee1902-avaliacao-massoterapia.appspot.com",
  messagingSenderId: "562073708771",
  appId: "1:562073708771:web:1136c007bdb1053463bdd6"
};

firebase.initializeApp(firebaseConfig);

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, AngularFirestoreModule, AngularFireAuthModule, HttpClientModule],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
