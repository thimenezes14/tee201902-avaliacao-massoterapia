import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-profissional',
  templateUrl: './profissional.page.html',
  styleUrls: ['./profissional.page.scss'],
})
export class ProfissionalPage implements OnInit {
  
  listaProfissionais: any;

  constructor(private navCtrl: NavController) { }

  ngOnInit() {
    fetch('../../../assets/servicos/lista-profissionais.json')
    .then(res => res.json())
    .then(json => {
      this.listaProfissionais = json;
    });
  }

  escolherProfissional(profissional: any) {
    const info = JSON.parse(localStorage.getItem('infoServico'));
    const servico = info.servico;
    let infoProfissional = {servico, profissional};
    localStorage.setItem('infoServico', JSON.stringify(infoProfissional));
   
    this.navCtrl.navigateForward('agendar-servico/data-agendamento');
  }

}
