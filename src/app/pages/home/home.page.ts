import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  constructor(private navCtrl: NavController) {}

  ngOnInit() {
    localStorage.removeItem('auth');
    localStorage.removeItem('infoServico');
  }

  loginFacebook() {
    let provider = new auth.FacebookAuthProvider();
    
    auth().signInWithPopup(provider).then(result => {
      let user = result.user;
      localStorage.setItem('auth', JSON.stringify(user));
      this.navCtrl.navigateForward('/agendar-servico');
    });
  }
}
