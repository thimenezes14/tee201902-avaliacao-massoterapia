import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-agendar-servico',
  templateUrl: './agendar-servico.page.html',
  styleUrls: ['./agendar-servico.page.scss'],
})
export class AgendarServicoPage implements OnInit {
  
  listaServicos: any;

  constructor(http: HttpClient, private navCtrl: NavController) {
    
  }

  ngOnInit() {
    fetch('../../../assets/servicos/lista-servicos.json')
    .then(res => res.json())
    .then(json => {
      this.listaServicos = json;
    });
  }

  escolherServico(servico: any) {
    let infoServico = {servico};
    
    localStorage.setItem('infoServico', JSON.stringify(infoServico));
   
    this.navCtrl.navigateForward('agendar-servico/profissional');
  }

}
