import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AgendarServicoPage } from './agendar-servico.page';

describe('AgendarServicoPage', () => {
  let component: AgendarServicoPage;
  let fixture: ComponentFixture<AgendarServicoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgendarServicoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AgendarServicoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
