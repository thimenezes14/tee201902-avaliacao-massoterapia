import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-data-agendamento',
  templateUrl: './data-agendamento.page.html',
  styleUrls: ['./data-agendamento.page.scss'],
})
export class DataAgendamentoPage implements OnInit {

  dataAtual = new Date();
  calcAgendamento = this.dataAtual.setDate(this.dataAtual.getDate() + 30);

  dataAtualAgendamento = new Date().toISOString();
  dataMaximaAgendamento = new Date(this.calcAgendamento).toISOString();

  diaAgd = this.dataAtualAgendamento; 
  horaAgd = '2019-12-31T12:00Z';

  constructor(private navCtrl: NavController) {}

  ngOnInit() {
  }

  escolherDataHora() {
    const info = JSON.parse(localStorage.getItem('infoServico'));
    
    const diaAgendamento = {
      data: this.diaAgd.slice(0, 10),
      hora: this.horaAgd.slice(11, 16)
    }
    const servico = info.servico;
    const profissional = info.profissional;
   
    let infoDataHora = {servico, profissional, diaAgendamento};
    localStorage.setItem('infoServico', JSON.stringify(infoDataHora));
   
    this.navCtrl.navigateForward('agendar-servico/pagamento');
  }

}
