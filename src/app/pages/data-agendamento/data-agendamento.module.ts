import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { Routes, RouterModule } from '@angular/router';
import { DataAgendamentoPage } from './data-agendamento.page';
import { ToolbarModule } from 'src/app/shared/components/toolbar.module';

const routes: Routes = [
  {
    path: '',
    component: DataAgendamentoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ToolbarModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DataAgendamentoPage]
})

export class DataAgendamentoPageModule {}
