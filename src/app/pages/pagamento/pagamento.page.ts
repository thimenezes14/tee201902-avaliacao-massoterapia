import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-pagamento',
  templateUrl: './pagamento.page.html',
  styleUrls: ['./pagamento.page.scss'],
})
export class PagamentoPage implements OnInit {
  nome: string;
  email: string;
  telefone: string;
  endereco: string;
  pagamento: string;
  valido: boolean = false;

  constructor(private navCtrl: NavController, private alert: AlertController) { 
    const data = JSON.parse(localStorage.getItem('auth'));
    this.nome = data.displayName;
    this.email = data.email;
  }

  ngOnInit() {
  }

  private validarInformacoes() {
    if(!RegExp(/^[a-zà-úA-ZÀ-Ú ]+$/).test(this.nome))
      return false;
    if(!RegExp(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/).test(this.email))
      return false;
    if(!RegExp(/^[0-9]{10,11}$/).test(this.telefone))
      return false;
      if(!RegExp(/^[a-zà-úA-ZÀ-Ú0-9-, ]+$/).test(this.endereco))
      return false;
    if(this.pagamento.length == 0 || !this.pagamento)
      return false;

    return true;
  }

  private async informarDadosDeAgendamento() {
    const data = JSON.parse(localStorage.getItem('infoServico'));
    const form = await this.alert.create({
        header: 'CONSULTA AGENDADA!',
        message: 
        '<strong>Veja abaixo informações da sua consulta:</strong><br /><hr />' +
        'Paciente: <strong>' + this.nome + '</strong><br /><hr />' +
        'E-mail: <strong>' + this.email + '</strong><br /><hr />' +
        'Telefone: <strong>' + this.telefone + '</strong><br /><hr />' +
        'Endereço: <strong>' + this.endereco + '</strong><br /><hr />' +
        'Tipo de Sessão: <strong>' + data.servico.nome + '</strong><br /><hr />' +
        'Valor Pago: <strong>R$ ' + data.servico.valor + '</strong><br /><hr />' +
        'Dr(a).: <strong>' + data.profissional.nome + ' - ' + data.profissional.crefito + '</strong><br /><hr />' +
        'Data/Hora: <strong>' + data.diaAgendamento.data + ' às ' + data.diaAgendamento.hora + '</strong><br /><hr />',
        buttons: [
            {text: 'Cancelar'},
            {text: 'Ok!', handler: () => this.navCtrl.navigateRoot('/')}
        ]
    });

    form.present();
  
  }

  confirmarAgendamento() {
    if(this.validarInformacoes()){
      console.log("Uhuu!!!");
      this.valido = true;
      this.informarDadosDeAgendamento();
    } else {
      console.log("Erro!");
      this.valido = false;
    }
  }

}
