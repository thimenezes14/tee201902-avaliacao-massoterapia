import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PagamentoPage } from './pagamento.page';
import { ToolbarModule } from 'src/app/shared/components/toolbar.module';


const routes: Routes = [
  {
    path: '',
    component: PagamentoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ToolbarModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PagamentoPage]
})
export class PagamentoPageModule {}
